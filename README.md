# INfrep PONG

## Features

- La balle bouge toute seule
- La balle rebondit sur les bords haut et bas de l'écran
- La balle s'arrete si elle touche les bords gauche et droit de l'écran

- les raquettes bougent verticalement
- les raquettes s'arretent si elles touchent un bord
- chaque raquette bouge avec deux touches (player1 : `a` et `q`, player 2 : `p`et `m`)

- le jeu démarre quand on appuie sur `space`

- la balle rebondit sur les raquettes
