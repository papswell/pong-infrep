let $ball;
let $pad1;
let $pad2;
let $scene;

let vp1 = 0;
let vp2 = 0;

let vbx = 5;
let vby = -5;

function moveBall() {
  let position = {
    x: $ball.offsetLeft,
    y: $ball.offsetTop
  };

  const limitX = $scene.offsetWidth - $ball.offsetWidth;
  const limitY = $scene.offsetHeight - $ball.offsetHeight;

  if (position.x < 0) {
    vbx = 0;
    vby = 0;
    position.x = 0;
  }

  if (position.x > limitX) {
    vbx = 0;
    vby = 0;
    position.x = limitX;
  }

  if (position.y < 0) {
    position.y = 0;
    vby *= -1;
  }

  if (position.y > limitY) {
    position.y = limitY;
    vby *= -1;
  }

  $ball.style.left = position.x + vbx + "px";
  $ball.style.top = position.y + vby + "px";
}

function movePad($pad, v) {
  let position = $pad.offsetTop;
  const limit = $scene.offsetHeight - $pad.offsetHeight;
  if (position < 0) {
    v = 0;
    position = 0;
  }
  if (position > limit) {
    v = 0;
    position = limit;
  }
  $pad.style.top = position + v + "px";
}

document.addEventListener("keydown", function(event) {
  switch (event.code) {
    case "KeyQ": // move player 1 up
      vp1 = -10;
      break;
    case "KeyA": // move player 1 down
      vp1 = 10;
      break;
    case "KeyP": // move player 2 up
      vp2 = -10;
      break;
    case "Semicolon": // move player 2 down
      vp2 = 10;
      break;
  }
});

document.addEventListener("keyup", function(event) {
  console.log(event.code);

  switch (event.code) {
    case "KeyQ":
    case "KeyA":
      // stop player 1
      vp1 = 0;
      break;
    case "KeyP":
    case "Semicolon":
      vp2 = 0;
      // stop player 2
      break;
    case "Space":
      // start/pause the game
      break;
  }
});

document.addEventListener("DOMContentLoaded", function() {
  $ball = document.getElementById("ball");
  $pad1 = document.getElementById("pad1");
  $pad2 = document.getElementById("pad2");
  $scene = document.getElementById("pong");
});

function gameLoop() {
  moveBall();
  movePad($pad1, vp1);
  movePad($pad2, vp2);

  requestAnimationFrame(gameLoop);
}

requestAnimationFrame(gameLoop);
